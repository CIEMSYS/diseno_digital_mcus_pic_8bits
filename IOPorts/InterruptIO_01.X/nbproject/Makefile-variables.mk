#
# Generated - do not edit!
#
# NOCDDL
#
CND_BASEDIR=`pwd`
# default configuration
CND_ARTIFACT_DIR_default=dist/default/production
CND_ARTIFACT_NAME_default=InterruptIO_01.X.production.hex
CND_ARTIFACT_PATH_default=dist/default/production/InterruptIO_01.X.production.hex
CND_PACKAGE_DIR_default=${CND_DISTDIR}/default/package
CND_PACKAGE_NAME_default=interruptio01.x.tar
CND_PACKAGE_PATH_default=${CND_DISTDIR}/default/package/interruptio01.x.tar
# Simulator configuration
CND_ARTIFACT_DIR_Simulator=dist/Simulator/production
CND_ARTIFACT_NAME_Simulator=InterruptIO_01.X.production.hex
CND_ARTIFACT_PATH_Simulator=dist/Simulator/production/InterruptIO_01.X.production.hex
CND_PACKAGE_DIR_Simulator=${CND_DISTDIR}/Simulator/package
CND_PACKAGE_NAME_Simulator=interruptio01.x.tar
CND_PACKAGE_PATH_Simulator=${CND_DISTDIR}/Simulator/package/interruptio01.x.tar

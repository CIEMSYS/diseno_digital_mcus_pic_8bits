/*******************************************************************************
 * FileName:            main.c
 * ProjectName:         LCD_01
 * Course:              Dise�o Digital con Microcontroladores PIC de 8 bits
 * Topic:               I/O Ports - LCD
 * Dependencies:        See INCLUDES section below
 * Processor:           PIC18F45K22
 * Compiler:            XC8, Ver. 2.05
 * Autor:               Sebasti�n Fernando Puente Reyes
 * e-mail:          	sebastian.puente@unillanos.edu.co
 * Date:            	Febrero de 2019
 *******************************************************************************
 * REQUERIMIENTO
 * Oscilador:
 * 
 * -Ver Descripcion.txt
 ******************************************************************************/

/*******************************************************************************
 * Librerias
 ******************************************************************************/
#include "ConfigurationBits.h"
#include "pic18_all.h"

/*******************************************************************************
 * Macros
 ******************************************************************************/

/*******************************************************************************
 * Prototipos de funciones
 ******************************************************************************/
void SetUp(void);

/*******************************************************************************
 * Variables globales
 ******************************************************************************/

/*******************************************************************************
 * Funci�n Principal
 ******************************************************************************/
void main(void)
{
    //Variables locales
    char TextoLCD[16];
    const char Mensaje[] = "Digitales II";
    char Caracter = 'A';
    uint16_t u16_a = 31;
    float b = 3.1416;
    
    //--Configutaciones Iniciales
    SetUp();
    Lcd_Init(); //Se inicializa la LCD
    
    Lcd_Cmd(LCD_CURSOR_OFF); //Se apaga el cursor
    Lcd_Out(1,0,Mensaje); //Imprimir un string en la l�nea 1 desde la posici�n 0
    Lcd_Cmd(LCD_MOVE_CURSOR_RIGHT); //Se desplaza a la derecha el cursor
    Lcd_Chr_CP(Caracter); //Imprimir caracter en la posici�n actual del cursor
    //Uso de sprintf para imprimir variables n�mericas
    sprintf(TextoLCD, "a=%d,b=%.3f",u16_a,b);
    Lcd_Out(2,0,TextoLCD); //Imprimir el resultado de sprintf en la seguna l�nea
       
    //--Ciclo Infinito, Tareas que el MCU hace indefinidamente
    while (1);
    
    return;
}

/*******************************************************************************
 * FUNCI�N:     SetUp()
 * ENTRADAS:	Ninguna
 * SALIDAS:     Ninguna
 * DESCRIPCI�N:	Configuraci�n inicial (oscilador, puertos, etc.)
 ******************************************************************************/
void SetUp(void)
{
    //---A---Configuraci�n Oscilador (Capitulo 2 DataSheet: Oscillator Module)

    //--A.1--Configuraci�n oscilador interno (si es el caso)
    OSCCONbits.IRCF = 0b111; //HFINTOSC = 16 MHz, Fosc = 16 MHz

    //--A.2--Habilitaci�n PLL (si es el caso)
    //OSCTUNEbits.PLLEN = 1; //Fosc = 4 x XX MHz = YY MHz

    //---B---Configuraci�n I/O Ports (Capitulo 10 DataSheet: I/O Ports)

    //--B.1--Inicializaci�n I/O Ports

    //--B.2--Habilitaci�n buffer entrada digital para las lineas digitales/anal�gicas
    //(Seccion 10.7 Datasheet: Port Analog Control)
    //ANSEL: PORT Analog Select Register
    //S�lo es necesaria cuando se desee configurar dichos pines como entradas digitales

    //--B.3--Sentido I/O Ports

    return;
}

/*******************************************************************************
 * DISE�O DIGITAL CON MICROCONTROLADORES PIC DE 8 BITS
 * Sebast�n Puente Reyes, M.Sc.
 * - CIEMSYS -
 * Semillero de Investigaci�n en Inteligencia Computacional y Sistemas Embebidos
 * Escuela de Ingenier�a
 * Facultad de Cienc�as B�sicas e Ingenier�a
 * Universidad de los Llanos
 * Villavicencio - Meta, Colombia
 ******************************************************************************/
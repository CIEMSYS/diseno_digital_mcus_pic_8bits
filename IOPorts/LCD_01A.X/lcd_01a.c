/*******************************************************************************
 * FileName:        lcd_01a.c
 * ProjectName:     LCD_01A
 * Course:          Dise�o Digital con Microcontroladores PIC de 8 bits
 * Topic:           I/O Ports - LCD
 * Dependencies:    See INCLUDES section below
 * Processor:       PIC18F45K22
 * Compiler:        XC8, Ver. 2.05
 * Author:          Sebasti�n Fernando Puente Reyes
 * e-mail:          sebastian.puente@unillanos.edu.co
 * Date:            Marzo de 2019
 *******************************************************************************
 * REQUERIMIENTO
 * -Ver Descripcion.txt
 ******************************************************************************/


/**
 * Secci�n: Archivos Incluidos
 */

#include <xc.h>
#include <stdint.h>
#include <stdio.h>
#include "ConfigurationBits.h"

/**
 * Secci�n: Macros
 */

#define _XTAL_FREQ (16000000) //Macro necesario para __delay_ms(), etc
//Macros para los pines de conexi�n de la LCD
#define     LCD_RD7     LATDbits.LATD7       // D7
#define     TRISRD7     TRISDbits.TRISD7
#define     LCD_RD6     LATDbits.LATD6       // D6
#define     TRISRD6     TRISDbits.TRISD6
#define     LCD_RD5     LATDbits.LATD5       // D5
#define     TRISRD5     TRISDbits.TRISD5
#define     LCD_RD4     LATDbits.LATD4       // D4
#define     TRISRD4     TRISDbits.TRISD4
#define     LCD_EN      LATEbits.LATE0       // EN
#define     TRISEN      TRISEbits.TRISE0
#define     LCD_RS      LATEbits.LATE1       // RS
#define     TRISRS      TRISEbits.TRISE1
//Macros para los comandos disponibles
#define     LCD_FIRST_ROW           128
#define     LCD_SECOND_ROW          192
#define     LCD_THIRD_ROW           148
#define     LCD_FOURTH_ROW          212
#define     LCD_CLEAR               1
#define     LCD_RETURN_HOME         2
#define     LCD_CURSOR_OFF          12
#define     LCD_BLINK_CURSOR_OFF    14
#define     LCD_BLINK_CURSOR_ON     15
#define     LCD_MOVE_CURSOR_LEFT    16
#define     LCD_MOVE_CURSOR_RIGHT   20
#define     LCD_TURN_OFF            0
#define     LCD_TURN_ON             8
#define     LCD_SHIFT_LEFT          24
#define     LCD_SHIFT_RIGHT         28

/**
 * Secci�n: Prototipos de funciones
 */

/**
 * @Resumen
 * A. Configuraci�n oscilador interno.
 * B. Configuraci�n Puertos I/O.
 */
void SetUp(void);
/**
 * Inicializaci�n de la LCD (puertos, etc)
 */
void Lcd_Init(void);
/**
 * Imprimir en la lCD un string de caracteres tipo const
 * @param y: N�mero de l�nea
 * @param x: N�mero de posici�n
 * @param buffer: Sring de caracteres
 */
void Lcd_Out(unsigned char y, unsigned char x, const char *buffer);
/**
 * Imprimir en la lCD un string de caracteres
 * @param y: N�mero de l�nea
 * @param x: N�mero de posici�n
 * @param buffer: Strung de caracteres
 */
void Lcd_Out2(unsigned char y, unsigned char x, char *buffer);
/**
 * Imprimir un caracter en la posici�n actual del cursor
 * @param data: caracter
 */
void Lcd_Chr_CP(char data);
/**
 * Enviar un comando a la LCD (Ver macros de comandos)
 * @param data: C�digo comando
 */
void Lcd_Cmd(unsigned char data);

/**
 * Secci�n: Funci�n Principal
 */
void main(void)
{
    //--Variables locales
    char TextoLCD[16]; //String donde imprime sprintf
    const char Mensaje[] = "Digitales II"; //String de caracteres tipo const
    char Caracter = 'A';
    uint16_t u16_a = 31;
    float b = 3.1416;
    
    //--Configutaciones Iniciales
    SetUp();
    Lcd_Init(); //Inicializaci�n LCD
    
    Lcd_Cmd(LCD_CURSOR_OFF); //Se apaga el cursor
    Lcd_Out(1,0,Mensaje); //Imprimir un string en la l�nea 1 desde la posici�n 0
    Lcd_Cmd(LCD_MOVE_CURSOR_RIGHT); //Se desplaza a la derecha el cursor
    Lcd_Chr_CP(Caracter); //Imprimir caracter en la posici�n actual del cursor
    //Uso de sprintf para imprimir variables n�mericas
    sprintf(TextoLCD, "a=%d,b=%.3f",u16_a,b);
    Lcd_Out(2,0,TextoLCD); //Imprimir el resultado de sprintf en la seguna l�nea
       
    //--Ciclo Infinito, Tareas que el MCU hace indefinidamente
    while (1);

    return;
}

/**
 * Secci�n: Desarrollo de Funciones
 */
//******************************************************************************
void SetUp(void)
{
    //---A---Configuraci�n Oscilador (Capitulo 2 DataSheet: Oscillator Module)

    //--A.1--Configuraci�n oscilador interno (si es el caso)
    OSCCONbits.IRCF = 0b111; //HFINTOSC = 16 MHz, Fosc = 16 MHz

    //--A.2--Habilitaci�n PLL (si es el caso)
    //OSCTUNEbits.PLLEN = 1; //Fosc = 4 x XX MHz = YY MHz

    //---B---Configuraci�n I/O Ports (Capitulo 10 DataSheet: I/O Ports)

    //--B.1--Inicializaci�n I/O Ports

    //--B.2--Habilitaci�n buffer entrada digital para las lineas digitales/anal�gicas
    //(Seccion 10.7 Datasheet: Port Analog Control)
    //ANSEL: PORT Analog Select Register
    //S�lo es necesaria cuando se desee configurar dichos pines como entradas digitales

    //--B.3--Sentido I/O Ports

    return;
}
//******************************************************************************
void Lcd_Init(void)
{
    unsigned char data;
    TRISRD7 = 0;
    TRISRD6 = 0;
    TRISRD5 = 0;
    TRISRD4 = 0;
    TRISEN = 0;
    TRISRS = 0;
    LCD_RD7 = 0;
    LCD_RD6 = 0;
    LCD_RD5 = 0;
    LCD_RD4 = 0;
    LCD_EN = 0;
    LCD_RS = 0;
    __delay_us(5500);
    __delay_us(5500);
    __delay_us(5500);
    __delay_us(5500);
    __delay_us(5500);
    __delay_us(5500);
    
    for(data = 1; data < 4; data ++)
    {
        LCD_RD7 = 0; LCD_RD6 = 0; LCD_RD5 = 1; LCD_RD4 = 1; LCD_EN = 0;
        LCD_RS = 0; LCD_RD7 = 0; LCD_RD6 = 0; LCD_RD5 = 1; LCD_RD4 = 1;
        LCD_EN = 1; LCD_RS = 0;
        __delay_us(5);
        LCD_RD7 = 0; LCD_RD6 = 0; LCD_RD5 = 1; LCD_RD4 = 1; LCD_EN = 0;
        LCD_RS = 0;
        __delay_us(5500);
    }
    LCD_RD7 = 0; LCD_RD6 = 0; LCD_RD5 = 1; LCD_RD4 = 0; LCD_EN = 0; LCD_RS = 0;
    LCD_RD7 = 0; LCD_RD6 = 0; LCD_RD5 = 1; LCD_RD4 = 0; LCD_EN = 1; LCD_RS = 0;
    __delay_us(5);
    LCD_RD7 = 0; LCD_RD6 = 0; LCD_RD5 = 1; LCD_RD4 = 0; LCD_EN = 0; LCD_RS = 0;
    __delay_us(5500);
    data = 40; Lcd_Cmd(data);
    data = 16; Lcd_Cmd(data);
    data = 1;  Lcd_Cmd(data);
    data = 15; Lcd_Cmd(data);
}
//******************************************************************************
void Lcd_Out(unsigned char y, unsigned char x, const char *buffer)
{
    unsigned char data;

    switch (y)
    {
        case 1: data = 128 + x; break;
        case 2: data = 192 + x; break;
        case 3: data = 148 + x; break;
        case 4: data = 212 + x; break;
        default: break;
    }
    Lcd_Cmd(data);
    while(*buffer)              // Write data to LCD up to null
    {
        Lcd_Chr_CP(*buffer);
        buffer++;               // Increment buffer
    }
    return;
}
//******************************************************************************
void Lcd_Out2(unsigned char y, unsigned char x, char *buffer)
{
    unsigned char data;

    switch (y)
    {
        case 1: data = 128 + x; break;
        case 2: data = 192 + x; break;
        case 3: data = 148 + x; break;
        case 4: data = 212 + x; break;
        default: break;
    }
    Lcd_Cmd(data);
    while(*buffer)              // Write data to LCD up to null
    {
        Lcd_Chr_CP(*buffer);
        buffer++;               // Increment buffer
    }
    return;
}
//******************************************************************************
void Lcd_Chr_CP(char data)
{
    LCD_EN = 0; LCD_RS = 1;
    LCD_RD7 = (data & 0b10000000)>>7; LCD_RD6 = (data & 0b01000000)>>6;
    LCD_RD5 = (data & 0b00100000)>>5; LCD_RD4 = (data & 0b00010000)>>4;
    _delay(10);
    LCD_EN = 1; __delay_us(5); LCD_EN = 0;
    LCD_RD7 = (data & 0b00001000)>>3; LCD_RD6 = (data & 0b00000100)>>2;
    LCD_RD5 = (data & 0b00000010)>>1; LCD_RD4 = (data & 0b00000001);
    _delay(10);
    LCD_EN = 1; __delay_us(5); LCD_EN = 0;
    __delay_us(5); __delay_us(5500);
}
//******************************************************************************
void Lcd_Cmd(unsigned char data)
{
    LCD_EN = 0; LCD_RS = 0;
    LCD_RD7 = (data & 0b10000000)>>7; LCD_RD6 = (data & 0b01000000)>>6;
    LCD_RD5 = (data & 0b00100000)>>5; LCD_RD4 = (data & 0b00010000)>>4;
    _delay(10);
    LCD_EN = 1; __delay_us(5); LCD_EN = 0;
    LCD_RD7 = (data & 0b00001000)>>3; LCD_RD6 = (data & 0b00000100)>>2;
    LCD_RD5 = (data & 0b00000010)>>1; LCD_RD4 = (data & 0b00000001);
    _delay(10);
    LCD_EN = 1; __delay_us(5); LCD_EN = 0;
    __delay_us(5500);//Delay_5us();
}
//******************************************************************************

/*******************************************************************************
 * DISE�O DIGITAL CON MICROCONTROLADORES PIC DE 8 BITS
 * Sebast�n Puente Reyes, M.Sc.
 * - CIEMSYS -
 * Semillero de Investigaci�n en Inteligencia Computacional y Sistemas Embebidos
 * Escuela de Ingenier�a
 * Facultad de Cienc�as B�sicas e Ingenier�a
 * Universidad de los Llanos
 * Villavicencio - Meta, Colombia
 ******************************************************************************/
//----------------------------------ciemsys-------------------------------------
/*
 * File:        ProgrammedIO_01.c
 * ProjectName: ProgrammedIO_01
 * Course:      Dise�o Digital con Microcontroladores PIC de 8 bits
 * Topic:       I/O Ports - Unconditional Programmed I/O
 * Processor:   PIC18F45K22
 * Compiler:    XC8, Ver.
 * Author:      Sebasti�n Fernando Puente Reyes, M.Sc.
 * E-mail:      sebastian.puente@unillanos.edu.co
 * Date:        Marzo de 2019
 */
//----------------------------------ciemsys-------------------------------------
/**
 * REQUERIMIENTO
 * --
 * Oscilador:
 *  Interno a 16 MHz (HFINTOSC)
 *  Fosc = 16 MHz, Tosc = 1/Fosc = 0.0625 useg
 *  Tcy = 4/Fosc = 4*Tosc = 0.25 useg
 * --
 * Descripci�n:
 *  Encender el LED D1 conectado en la l�nea RD0
 * --
 * Ver Descripcion.txt
 */
//----------------------------------ciemsys-------------------------------------

/**
 * Secci�n: Archivos incluidos
 */

#include <xc.h>
#include "ConfigurationBits.h"

/**
 * Secci�n: Definiciones (macros)
 */

/**
 * Secci�n: Prototipos de funciones
 */

/**
 * Secci�n: Variables globales
 */

/**
 * Secci�n: Funci�n Principal (main)
 */

void main(void)
{
    // --Configuraciones Iniciales

    // Configuraci�n Oscilador (Capitulo 2 DataSheet: Oscillator Module)
    // Configuraci�n oscilador interno
    OSCCONbits.IRCF = 0b111; // HFINTOSC = 16 MHz, Fosc = 16 MHz

    // Configuraci�n I/O Ports (Capitulo 10 I/O Ports DataSheet)
    LATD = 0; // Inicializar PORTD, tambien es v�lido: PORTD = 0;
    TRISDbits.TRISD0 = 0; // L�nea RD0 como salida (para el LED D1)

    // --Ciclo Infinito, Tareas que el MCU hace indefinidamente
    while (1)
    {
        // Encender LED D1 conectado en RD0
        PORTDbits.RD0 = 1; // Nivel alto por RD0
    }
}

/* *****************************************************************************
 * DISE�O DIGITAL CON MICROCONTROLADORES PIC DE 8 BITS
 * Profesor: Sebastian Puente Reyes, M.Sc.
 * - CIEMSYS -
 * Semillero de Investigaci�n en Inteligencia Computacional y Sistemas Embebidos
 * Escuela de Ingenier�a
 * Facultad de Cienc�as B�sicas e Ingenier�a
 * Universidad de los Llanos
 * Villavicencio - Meta, Colombia
 * *****************************************************************************
 */
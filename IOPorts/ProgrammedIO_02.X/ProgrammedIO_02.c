//----------------------------------ciemsys-------------------------------------
/*
 * File:        ProgrammedIO_02.c
 * ProjectName: ProgrammedIO_02
 * Course:      Dise�o Digital con Microcontroladores PIC de 8 bits
 * Topic:       I/O Ports - Unconditional Programmed I/O
 * Processor:   PIC18F45K22
 * Compiler:    XC8, Ver.
 * Author:      Sebasti�n Fernando Puente Reyes, M.Sc.
 * E-mail:      sebastian.puente@unillanos.edu.co
 * Date:        Octubre de 2018
 */
//----------------------------------ciemsys-------------------------------------
/**
 * REQUERIMIENTO
 * --
 * Oscilador:
 *  Interno a 16 MHz (HFINTOSC), PLL habilitada
 *  Fosc = (4x16)MHz = 64 MHz, Tosc = 1/Fosc = 15.625 nseg (0.015625 useg)
 *  Tcy = 4/Fosc = 4*Tosc = 62.5 nseg (0.0625 useg)
 * --
 * Descripci�n:
 *  Encender el LED D1, conectado en RD0, a trav�s del Switch SW_S1 conectado
 *  en la l�nea RB0 en logica negativa ("1" abierto, "0" cerrado).
 *  El LED D1 se deber� encender cuando SW_S1 sea cerrado y apagarse en el
 *  caso contrario.
 * --
 * Ver Descripcion.txt
 */
//----------------------------------ciemsys-------------------------------------

/**
 * Secci�n: Archivos Incluidos
 */

#include <xc.h> //Lib con informaci�n del MCU
#include "ConfigurationBits.h" //Bits de configuraci�n

/**
 * Secci�n: Definiciones (macros)
 */

//#define _XTAL_FREQ XX000000 //Macro necesario para __delay_ms()
#define LED_D1  (LATDbits.LATD0)

/**
 * Secci�n: Prototipos de Funciones
 */

/**
 * SysInitialize()
 * @resumen
 *  A. Configuraci�n oscilador
 *  B. Configuraci�n puertos I/O
 */
void SysInitialize(void); // Configuraci�n inicial

/**
 * Secci�n: Variables Globales
 */

/**
 * Secci�n: Funci�n Principal (main)
 */

void main(void)
{
    // --Configuraciones Iniciales
    SysInitialize();

    // --Ciclo Infinito, Tareas que el MCU hace indefinidamente
    while(1)
    {
        /* El nivel (estado) que entrega SW_S1 en RB0 es complementado y
         * sacado por la l�nea RD0 */
        LATDbits.LATD0 = ~PORTBbits.RB0;
        
        // Tambien se puede hacer lo anterior usando el macro creado LED_D1
        //LED_D1 = ~PORTBbits.RB0;
    }
    
    return;
}

/**
 * Secci�n: Desarrollo de Funciones
 */

//----------------------------------ciemsys-------------------------------------
void SysInitialize(void)
{
    // ---A---Configuraci�n Oscilador (Capitulo 2 DataSheet: Oscillator Module)

    // --A.1--Configuraci�n oscilador interno (si es el caso)
    OSCCONbits.IRCF = 0b111; // HFINTOSC = 16 MHz, Fosc = 16 MHz
    
    // --A.2--Habilitaci�n PLL (si es el caso)
    OSCTUNEbits.PLLEN = 1; // Fosc = 4 x 16 MHz = 64 MHz
    
    // ---B---Configuraci�n I/O Ports (Capitulo 10 DataSheet: I/O Ports)

    // --B.1--Inicializaci�n I/O Ports
    LATB = 0; // Inicializar PORTB, tambien es v�lido: PORTB = 0;
    LATD = 0; // Inicializar PORTD
    
    
    /* --B.2--Habilitaci�n buffer entrada digital de lineas digitales/anal�gicas
     * (Seccion 10.7 Datasheet: Port Analog Control)
     * S�lo cuando se desee configurar dichos pines como entradas digitales
     * ANSELB: PORTB Analog Select Register (ANSB<5:0>: RB<5:0> Analog Select bit)
     */
    ANSELBbits.ANSB0 = 0; // Habilitaci�n buffer entrada digital de RB0 (SW_S1)

    // --B.2--Sentido I/O Ports
    TRISBbits.TRISB0 = 1;   // L�nea RB0 como entrada (para SW_S1)
    TRISDbits.TRISD0= 0;    // L�nea RD0 como salida (para LED D1)
    
    return;
}

/* *****************************************************************************
 * DISE�O DIGITAL CON MICROCONTROLADORES PIC DE 8 BITS
 * Profesor: Sebastian Puente Reyes, M.Sc.
 * - CIEMSYS -
 * Semillero de Investigaci�n en Inteligencia Computacional y Sistemas Embebidos
 * Escuela de Ingenier�a
 * Facultad de Cienc�as B�sicas e Ingenier�a
 * Universidad de los Llanos
 * Villavicencio - Meta, Colombia
 * *****************************************************************************
 */
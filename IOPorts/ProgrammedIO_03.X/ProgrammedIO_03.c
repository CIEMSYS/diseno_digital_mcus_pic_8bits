//----------------------------------ciemsys-------------------------------------
/*
 * File:        ProgrammedIO_03.c
 * ProjectName: ProgrammedIO_03
 * Course:      Dise�o Digital con Microcontroladores PIC de 8 bits
 * Topic:       I/O Ports - Unconditional Programmed I/O
 * Processor:   PIC18F45K22
 * Compiler:    XC8, Ver.
 * Author:      Sebasti�n Fernando Puente Reyes, M.Sc.
 * E-mail:      sebastian.puente@unillanos.edu.co
 * Date:        Octubre de 2018
 */
//----------------------------------ciemsys-------------------------------------
/**
 * REQUERIMIENTO
 * --
 * Oscilador:
 *  Interno a 16 MHz (HFINTOSC)
 *  Fosc = (4x16)MHz = 64 MHz, Tosc = 1/Fosc = 15.625 nseg (0.015625 useg)
 *  Tcy = 4/Fosc = 4*Tosc = 62.5 nseg (0.0625 useg)
 * --
 * Descripci�n:
 *  Generar una se�al cuadrada por la l�nea RD0 con un periodo (T) de 2 seg y
 *  un ciclo de trabajo del 50%. Es decir, la se�al se mantiene en estado ALTO
 *  por 1 seg (0.5*2seg) y luego pasa a estado BAJO por 1 seg (0.5*2seg).
 * --
 * Ver Descripcion.txt
 */
//----------------------------------ciemsys-------------------------------------

/**
 * Secci�n: Archivos Incluidos
 */

#include <xc.h> // Lib con informaci�n del MCU
#include "ConfigurationBits.h" // Bits de configuraci�n


/**
 * Secci�n: Definiciones (macros)
 */

#define _XTAL_FREQ  (16000000) // Macro para __delay_us(x), __delay_ms(x)

/**
 * Secci�n: Prototipos de Funciones
 */

/**
 * SysInitialize()
 * @resumen
 *  A. Configuraci�n oscilador
 *  B. Configuraci�n puertos I/O
 */
void SysInitialize(void); // Configuraci�n inicial

/**
 * Secci�n: Variables globales
 */

bit LED = 0; // Variable tipo bit

/**
 * Secci�n: Funci�n Principal (main)
 */

void main(void)
{
    // --Configuraciones Iniciales
    SysInitialize();

    // --Ciclo Infinito, Tareas que el MCU hace indefinidamente
    while(1)
    {
        LED = !LED; // Se complementa la variable LED
        LATDbits.LATD0 = LED; // El valor de la variable LED se muestra por RD0
        __delay_ms(1000); // Funci�n para temporizaciones en ms, se pide un retardo de 1000 ms = 1seg
    }

    return;
}

/**
 * Secci�n: Desarrollo de Funciones
 */

//----------------------------------ciemsys-------------------------------------
void SysInitialize(void)
{
    // ---A---Configuraci�n Oscilador (Capitulo 2 DataSheet: Oscillator Module)

    // --A.1--Configuraci�n oscilador interno (si es el caso)
    OSCCONbits.IRCF = 0b111; // HFINTOSC = 16 MHz, Fosc = 16 MHz

    // ---B---Configuraci�n I/O Ports (Capitulo 10 DataSheet: I/O Ports)

    // --B.1--Inicializaci�n I/O Ports
    LATD = 0; // Inicializar PORTD

    // --B.2--Sentido I/O Ports
    TRISDbits.TRISD0 = 0; // L�nea RD0 como salida
    
    return;
}

/* *****************************************************************************
 * DISE�O DIGITAL CON MICROCONTROLADORES PIC DE 8 BITS
 * Profesor: Sebastian Puente Reyes, M.Sc.
 * - CIEMSYS -
 * Semillero de Investigaci�n en Inteligencia Computacional y Sistemas Embebidos
 * Escuela de Ingenier�a
 * Facultad de Cienc�as B�sicas e Ingenier�a
 * Universidad de los Llanos
 * Villavicencio - Meta, Colombia
 * *****************************************************************************
 */
#
# Generated - do not edit!
#
# NOCDDL
#
CND_BASEDIR=`pwd`
# default configuration
CND_ARTIFACT_DIR_default=dist/default/production
CND_ARTIFACT_NAME_default=ProgrammedIO_03.X.production.hex
CND_ARTIFACT_PATH_default=dist/default/production/ProgrammedIO_03.X.production.hex
CND_PACKAGE_DIR_default=${CND_DISTDIR}/default/package
CND_PACKAGE_NAME_default=programmedio03.x.tar
CND_PACKAGE_PATH_default=${CND_DISTDIR}/default/package/programmedio03.x.tar
# Simulator configuration
CND_ARTIFACT_DIR_Simulator=dist/Simulator/production
CND_ARTIFACT_NAME_Simulator=ProgrammedIO_03.X.production.hex
CND_ARTIFACT_PATH_Simulator=dist/Simulator/production/ProgrammedIO_03.X.production.hex
CND_PACKAGE_DIR_Simulator=${CND_DISTDIR}/Simulator/package
CND_PACKAGE_NAME_Simulator=programmedio03.x.tar
CND_PACKAGE_PATH_Simulator=${CND_DISTDIR}/Simulator/package/programmedio03.x.tar

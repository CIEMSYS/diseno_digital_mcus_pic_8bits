//----------------------------------ciemsys-------------------------------------
/*
 * File:        ProgrammedIO_04.c
 * ProjectName: ProgrammedIO_04
 * Course:      Dise�o Digital con Microcontroladores PIC de 8 bits
 * Topic:       I/O Ports - Unconditional Programmed I/O
 * Processor:   PIC18F45K22
 * Compiler:    XC8, Ver.
 * Author:      Sebasti�n Fernando Puente Reyes, M.Sc.
 * E-mail:      sebastian.puente@unillanos.edu.co
 * Date:        Octubre de 2018
 */
//----------------------------------ciemsys-------------------------------------
/**
 * REQUERIMIENTO
 * --
 * Oscilador:
 *  Interno a 8 MHz (HFINTOSC), PLL habilitada
 *  Fosc = (4x8)MHz = 32 MHz, Tosc = 1/Fosc = 31.25 nseg (0.03125 useg)
 *  Tcy = 4/Fosc = 4*Tosc = 125 nseg (0.125 useg)
 * --
 * Descripci�n:
 *  Realizar un contador binario de 4 bits, 0 - 15, que se visualize a trav�s
 *  de las 4 l�neas menos significativas del Puerto D (RD<3:0>).
 *  El incremento debe ser cada 500 mseg.
 * --
 * Ver Descripcion.txt
 */
//----------------------------------ciemsys-------------------------------------

/**
 * Secci�n: Archivos Incluidos
 */

#include <xc.h> // Lib con informaci�n del MCU
#include <stdint.h> // Lib est�ndar para declaraci�n de variables enteras
#include "ConfigurationBits.h" // Bits de configuraci�n

/**
 * Secci�n: Definiciones (macros)
 */

#define _XTAL_FREQ  (32000000) // Macro necesario para __delay_ms()

/**
 * Secci�n: Prototipos de Funciones
 */

/**
 * SysInitialize()
 * @resumen
 *  A. Configuraci�n oscilador
 *  B. Configuraci�n puertos I/O
 */
void SysInitialize(void);

/**
 * Secci�n: Variables Globales
 */

uint8_t u8_Contador = 0; // Variable tipo entero sin signo de 8 bits

/**
 * Secci�n: Funci�n Principal (main)
 */

void main(void)
{
    uint8_t u8_i; // Variable tipo entero sin signo de 8 bits usada por el for

    // --Configuraciones Iniciales
    SysInitialize();

    // --Ciclo Infinito, Tareas que el MCU hace indefinidamente
    while(1)
    {
        for(u8_i = 0; u8_i <= 15; u8_i++)
        {
            u8_Contador = u8_i;
            LATD = u8_Contador;
            __delay_ms(500); // Retardo de medio segundo (500 mseg)
        }
    }
    
    return;
}

/**
 * Secci�n: Desarrollo de Funciones
 */

//----------------------------------ciemsys-------------------------------------
void SysInitialize(void)
{
    // ---A---Configuraci�n Oscilador (Capitulo 2 DataSheet: Oscillator Module)

    // --A.1--Configuraci�n oscilador interno
    OSCCONbits.IRCF = 0b110; // HFINTOSC = 8 MHz, Fosc = 8 MHz

    // --A.2--Habilitaci�n PLL
    OSCTUNEbits.PLLEN = 1; // Fosc = 4 x 8 MHz = 32 MHz

    // ---B---Configuraci�n I/O Ports (Capitulo 10 DataSheet: I/O Ports)

    // --B.1--Inicializaci�n I/O Ports
    LATD = 0; // Inicializar PORTD

    // --B.2--Sentido I/O Ports
    TRISD &= 0xF0; // L�neas RD<3:0> como salidas digitales
    
    return;
}

/* *****************************************************************************
 * DISE�O DIGITAL CON MICROCONTROLADORES PIC DE 8 BITS
 * Profesor: Sebastian Puente Reyes, M.Sc.
 * - CIEMSYS -
 * Semillero de Investigaci�n en Inteligencia Computacional y Sistemas Embebidos
 * Escuela de Ingenier�a
 * Facultad de Cienc�as B�sicas e Ingenier�a
 * Universidad de los Llanos
 * Villavicencio - Meta, Colombia
 * *****************************************************************************
 */
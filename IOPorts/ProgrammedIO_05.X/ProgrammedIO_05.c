//----------------------------------ciemsys-------------------------------------
/*
 * File:        ProgrammedIO_05.c
 * ProjectName: ProgrammedIO_05
 * Course:      Dise�o Digital con Microcontroladores PIC de 8 bits
 * Topic:       I/O Ports - Conditional Programmed I/O
 * Processor:   PIC18F45K22
 * Compiler:    XC8, Ver.
 * Author:      Sebasti�n Fernando Puente Reyes, M.Sc.
 * E-mail:      sebastian.puente@unillanos.edu.co
 * Date:        Octubre de 2018
 */
//----------------------------------ciemsys-------------------------------------
/**
 * REQUERIMIENTO
 * --
 * Oscilador:
 *  Cristal Externo, tipo HS-MP, de 10 MHz
 *  Fosc = 10 MHz, Tosc = 1/Fosc = 100 nseg (0.1 useg)
 *  Tcy = 4/Fosc = 4*Tosc = 400 nseg (0.4 useg)
 * --
 * Desrcripci�n:
 *
 * --
 * Ver Descripcion.txt
 */
//----------------------------------ciemsys-------------------------------------

/**
 * Secci�n: Archivos Incluidos
 */

#include <xc.h> // Lib con informaci�n del MCU
#include <stdint.h> // Lib est�ndar para enteros
#include "ConfigurationBits_HSMP.h" // Bits de configuraci�n

/**
 * Secci�n: Definiciones (macros)
 */

#define _XTAL_FREQ  (10000000) // Macro necesario para __delay_ms(), etc

#define SW_S1           (PORTBbits.RB0)
#define SW_S1_TRIS      (TRISBbits.TRISB0)
#define SW_S1_LAT       (LATB)
#define SW_S1_ANSEL     (ANSELBbits.ANSB0)
#define PRESSED         (0)

#define DICE        (LATD)
#define DICE_LAT    (LATD)
#define DICE_TRIS   (TRISD)

/**
 * Secci�n: Prototipos de Funciones
 */

/**
 * SysInitialize()
 * @resumen
 *  A. Configuraci�n oscilador
 *  B. Configuraci�n puertos I/O
 */
void SysInitialize(void);

/**
 * DelayMseg
 * @resumen
 *  Funci�n que genera un retardo en milisegundos
 * @parametro u16_n
 *  Milisegundos deseados
 */
void DelayMseg(uint16_t u16_n);

/**
 * Secci�n: Variables Globales
 */

// Arreglo para el manejo del dado formado por 7 LEDS (D7:D0)
//                     0    1    2    3    4    5    6
uint8_t u8_Dado[] = {0x00,0x08,0x41,0x49,0x55,0x5D,0x77};
// RD7-RD6-RD5-RD4-RD3-RD2-RD1-RD0
// -   D7  D6  D5  D4  D3  D2  D1 
// Disposici�n LEDS D7:D2 para el dado
//     D1    D5
//     D2 D4 D6
//     D3    D7

/**
 * Secci�n: Funci�n Principal (main)
 */

void main(void)
{
    // --Variables locales
    uint8_t u8_j = 1;
    
    // --Configuraciones Iniciales
    SysInitialize();

    // --Ciclo Infinito, Tareas que el MCU hace indefinidamente
    while(1)
    {
        // Para este caso el dado mostrar� n�meros del 1 al 6
        if(SW_S1 == PRESSED)
        {
            // Mientras se mantenga pulsado SW_S1 se estar� haciendo un retardo
            while(SW_S1 == PRESSED)
            {
                __delay_ms(1);
            }
            // Mostrar en el dado el valor de u8_j cuando se suelte SW_S1
            DICE = u8_Dado[u8_j];
            // Despues de 2 segundos se apaga el dado y se inicializa u8_j
            DelayMseg(2000); // Retardo de 2 seg.
            DICE = 0x00; // Se apaga el dado
            u8_j = 0;
        }
        u8_j++;
        __delay_ms(70);
        if(u8_j == 7)u8_j = 1;
    }
    return;
}

/**
 * Secci�n: Desarrollo de Funciones
 */

//----------------------------------ciemsys-------------------------------------
void SysInitialize(void)
{
    // ---A---Configuraci�n Oscilador (Capitulo 2 DataSheet: Oscillator Module)

    // --A.1--Configuraci�n oscilador interno
    // Se usa cristal externo tipo HS-MP (4-16 MHz) de 10 MHz, Fosc = 10 MHz

    // ---B---Configuraci�n I/O Ports (Capitulo 10 DataSheet: I/O Ports)

    // --B.1--Inicializaci�n I/O Ports
    SW_S1_LAT = 0;  // Inicializar PORT para el Switch SW_S1
    DICE_LAT = 0;   // Inicializar PORT para el dado

    
    /* --B.2--Habilitaci�n buffer entrada digital de lineas digitales/anal�gicas
     * (Seccion 10.7 Datasheet: Port Analog Control)
     * S�lo cuando se desee configurar dichos pines como entradas digitales
     * ANSELX: PORTX Analog Select Register
     */
    SW_S1_ANSEL = 0; // Habilitaci�n buffer entrada digital para SW_S1

    // --B.3--Sentido I/O Ports
    SW_S1_TRIS = 1;     // L�nea del SW_S1 como entrada
    DICE_TRIS = 0x00;   // L�neas del dado como salidas

    return;
}
//----------------------------------ciemsys-------------------------------------
void DelayMseg(uint16_t u16_n) // n -> milisegundos
{
    uint16_t u16_i;
    for (u16_i=0; u16_i < u16_n; u16_i++)
        __delay_ms(1);
}

/* *****************************************************************************
 * DISE�O DIGITAL CON MICROCONTROLADORES PIC DE 8 BITS
 * Profesor: Sebastian Puente Reyes, M.Sc.
 * - CIEMSYS -
 * Semillero de Investigaci�n en Inteligencia Computacional y Sistemas Embebidos
 * Escuela de Ingenier�a
 * Facultad de Cienc�as B�sicas e Ingenier�a
 * Universidad de los Llanos
 * Villavicencio - Meta, Colombia
 * *****************************************************************************
 */
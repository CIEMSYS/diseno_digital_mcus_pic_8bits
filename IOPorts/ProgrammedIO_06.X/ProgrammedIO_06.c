//----------------------------------ciemsys-------------------------------------
/*
 * File:        ProgrammedIO_06.c
 * ProjectName: ProgrammedIO_06
 * Course:      Dise�o Digital con Microcontroladores PIC de 8 bits
 * Topic:       I/O Ports - Conditional Programmed I/O
 * Processor:   PIC18F45K22
 * Compiler:    XC8, Ver.
 * Author:      Sebasti�n Fernando Puente Reyes, M.Sc.
 * E-mail:      sebastian.puente@unillanos.edu.co
 * Date:        Octubre de 2018
 */
//----------------------------------ciemsys-------------------------------------
/**
 * REQUERIMIENTO
 * --
 * Oscilador:
 *  Cristal Externo, tipo HS-MP, de 10 MHz, PLL habilitada
 *  Fosc = (4x10)MHz = 40 MHz, Tosc = 1/Fosc = 25 nseg (0.025 useg)
 *  Tcy = 4/Fosc = 4*Tosc = 100 nseg (0.1 useg)
 * --
 * Descripci�n:
 *  Se captura n�mero de 4 bits (0-15) por las l�neas RB<7:4> (DipSwitch DSW1)
 *  Si el n�mero esta entre 0-9 se visualiza en un display 7 segmentos c�todo
 *  com�n, conectado en el PORTD, de lo contrario no se visualiza nada.
 * --
 * Ver Descripcion.txt
 */
//----------------------------------ciemsys-------------------------------------

/**
 * Secci�n: Archivos Incluidos
 */

#include <xc.h>
#include <stdint.h> // Lib est�ndar para enteros
#include "ConfigurationBits_HSMP.h"

/**
 * Secci�n: Definiciones (macros)
 */
//#define _XTAL_FREQ XX000000 //Macro necesario para __delay_ms(), etc

/**
 * Secci�n: Protipos de Funciones
 */

/**
 * SysInitialize()
 * @resumen
 *  A. Configuraci�n oscilador
 *  B. Configuraci�n puertos I/O
 */
void SysInitialize(void);

/**
 * Secci�n: Variables Globales
 */

// Arreglo para el manejo de un display 7 segmentos de c�todo com�n
//                          0     1     2     3     4     5     6     7     8     9
uint8_t u8_Disp7SegCC[] = {0x3F, 0x06, 0x5B, 0x4F, 0x66, 0x6D, 0x7D, 0x07, 0x7F, 0x6F};

uint8_t u8_DipSwitch = 0x00; // Variable para almacenar valor del DipSwitch DSW1

/**
 * Secci�n: Funci�n Principal (main)
 */

void main(void)
{ 
    // --Configuraciones Iniciales
    SysInitialize();
    
    // Activaci�n del display 7 seg de unidades y desactivaci�n de los dem�s
    LATCbits.LATC2 = 0; // Desactivaci�n display unidades de mil
    LATCbits.LATC3 = 0; // Desactivaci�n display centenas
    LATCbits.LATC4 = 0; // Desactivaci�n display decenas
    LATCbits.LATC5 = 1; // Activaci�n display unidades

    // --Ciclo Infinito, Tareas que el MCU hace indefinidamente
    while (1)
    {
        // Como los interruptores del DipSwitch DSW1 estan con l�gica negativa
        // se captura el valor del PORTB y se complementa
        u8_DipSwitch = ~PORTB;
        u8_DipSwitch = u8_DipSwitch >> 4;

        // En �ste punto u8_Interruptores tendr� s�lo un valor entre 0 y 15
        // Para este caso el display mostrar� n�meros del 0 al 9
        if (u8_DipSwitch < 10)
        {
            LATD = u8_Disp7SegCC[u8_DipSwitch];
        }
        else // Para >= 10 no se muestra nada en el display
        {
            LATD = 0x00; // Se apagan todos los segmentos del display
        }
    }
    return;
}

/**
 * Secci�n: Desarrollo de Funciones
 */

//----------------------------------ciemsys-------------------------------------
void SysInitialize(void)
{
    // ---A---Configuraci�n Oscilador (Capitulo 2 DataSheet: Oscillator Module)

    // --A.1--Configuraci�n oscilador interno
    // Se usa cristal externo tipo HS-MP (4-16 MHz) de 10 MHz
    
    // --A.2--Habilitaci�n PLL
    OSCTUNEbits.PLLEN = 1; // Fosc = 4 x 10 MHz = 40 MHz

    // ---B---Configuraci�n I/O Ports (Capitulo 10 DataSheet: I/O Ports)

    // --B.1--Inicializaci�n I/O Ports
    LATB = 0; // Inicializar PORTB
    LATC = 0; // Inicializar PORTC
    LATD = 0; // Inicializar PORTD

     /* --B.2--Habilitaci�n buffer entrada digital de lineas digitales/anal�gicas
     * (Seccion 10.7 Datasheet: Port Analog Control)
     * S�lo cuando se desee configurar dichos pines como entradas digitales
     * ANSELX: PORTX Analog Select Register
     */
    ANSELB &= 0b11001111; // Habilitaci�n buffer entrada digital l�neas RB<5:4>

    // --B.3--Sentido I/O Ports
    TRISB |= 0xF0; // L�neas RB<7:4> como entradas (DipSwitch DSW1)
    TRISC &= 0b11000011; // RC<5:2> como salidas (l�neas activaci�n displays)
    TRISD = 0x00; // Todas las l�neas del PORTD (RD<7:0>) como salidas (DISPLAY)

    return;
}

/* *****************************************************************************
 * DISE�O DIGITAL CON MICROCONTROLADORES PIC DE 8 BITS
 * Profesor: Sebastian Puente Reyes, M.Sc.
 * - CIEMSYS -
 * Semillero de Investigaci�n en Inteligencia Computacional y Sistemas Embebidos
 * Escuela de Ingenier�a
 * Facultad de Cienc�as B�sicas e Ingenier�a
 * Universidad de los Llanos
 * Villavicencio - Meta, Colombia
 * *****************************************************************************
 */
//----------------------------------ciemsys-------------------------------------
/*
 * File:        ProgrammedIO_07.c
 * ProjectName: ProgrammedIO_07
 * Course:      Dise�o Digital con Microcontroladores PIC de 8 bits
 * Topic:       I/O Ports - Conditional Programmed I/O
 * Processor:   PIC18F45K22
 * Compiler:    XC8, Ver.
 * Author:      Sebasti�n Fernando Puente Reyes, M.Sc.
 * E-mail:      sebastian.puente@unillanos.edu.co
 * Date:        Marzo de 2019
 */
//----------------------------------ciemsys-------------------------------------
/**
 * REQUERIMIENTO
 * --
 * Oscilador:
 *  Cristal Externo, tipo HS-MP, de 10 MHz, PLL habilitada
 *  Fosc = (4x10)MHz = 40 MHz, Tosc = 1/Fosc = 25 nseg (0.025 useg)
 *  Tcy = 4/Fosc = 4*Tosc = 100 nseg (0.1 useg)
 * --
 * Descripci�n:
 *  Se realiza un contador decimal en el rango de 0-9 con cuenta ascendente o
 *  descendente condicionada al switch SW_S1 conectado en la l�nea RB0:
 *      - Si no se pulsa SW_S1 la cuenta es descendente.
 *      - Si se pulsa SW_S1 la cuenta es ascedente.
 *  El tiempo entre cuenta es de 1 seg (1000 mseg)
 * --
 * Ver Descripci�n.txt
 */
//----------------------------------ciemsys-------------------------------------

/**
 * Secci�n: Archivos Incluidos
 */

#include <xc.h> // Lib con informaci�n del MCU
#include <stdint.h> // Lib est�ndar para enteros
#include "ConfigurationBits_HSMP.h" // Bits configuraci�n (incluir en proyecto)

/**
 * Secci�n: Definiciones (macros)
 */

#define _XTAL_FREQ 40000000 // Macro para __delay_ms(), __delay_us(), etc.

#define SW_S1           (PORTBbits.RB0)
#define SW_S1_TRIS      (TRISBbits.TRISB0)
#define SW_S1_LAT       (LATB)
#define SW_S1_ANSEL     (ANSELBbits.ANSB0)
#define PRESSED         (0)

#define DISPLAY             (LATD)
#define DISPLAY_TRIS        (TRISD)
#define DISPLAY_LAT         (LATD)
#define EN_DISPLAY_LAT      (LATC)
#define EN_DISPLAY_TRIS     (TRISC)
#define EN_UNITS_DISPLAY    (LATCbits.LATC5) // Habilitador Display Unidades
#define EN_TENS_DISPLAY     (LATCbits.LATC4) // Habilitador Display Decenas
#define EN_HND_DISPLAY      (LATCbits.LATC3) // Habilitador Display Centenas
#define EN_THSD_DISPLAY     (LATCbits.LATC2) // Habilitador Display Unidades Mil
#define DISP(x)             (DISPLAY = u8_Disp7SegCC[x])

/**
 * Secci�n: Prototipos de Funciones
 */

/**
 * SysInitialize()
 * @resumen
 *  A. Configuraci�n oscilador
 *  B. Configuraci�n puertos I/O
 */
void SysInitialize(void);

/**
 * delayms(uint16_t u16_ms)
 * @Resumen
 *  Funci�n que genera un delay en milisegundos
 * @Parametro u16_ms: milisegundos deseados
 */
void delayms(uint16_t u16_ms);

/**
 * Secci�n: Variables Globales
 */

uint8_t u8_Disp7SegCC[] = {0x3F, 0x06, 0x5B, 0x4F, 0x66, 0x6D, 0x7D, 0x07, 0x7F, 0x6F};

/**
 * Secci�n: Funci�n Principal
 */

void main(void)
{
    // --Variables locales
    uint8_t u8_aux = 0;

    // --Configuraciones Iniciales
    SysInitialize();

    // Activaci�n del display 7 seg de unidades y desactivaci�n de los dem�s
    EN_THSD_DISPLAY = 0;    // Deshabilitaci�n Display Unidades de Mil
    EN_HND_DISPLAY = 0;     // Deshabiitaci�n Display Centenas
    EN_TENS_DISPLAY = 0;    // Deshabilitaci�n Display Decenas
    EN_UNITS_DISPLAY = 1;   // Habilitaci�n Display Unidades

    // --Ciclo Infinito, Tareas que el MCU hace indefinidamente
    while (1)
    {
        DISP(u8_aux);
        delayms(1000); // No confundir con __delay_ms()

        if (SW_S1 == PRESSED)
        {
            u8_aux = (uint8_t) (u8_aux + 1);
            if (u8_aux > 9)
                u8_aux = 0;
        }
        else if (u8_aux == 0)
            u8_aux = 9;
        else
            u8_aux = (uint8_t) (u8_aux - 1);
    }

    return;
}

/**
 * Secci�n: Desarrollo de Funciones
 */

//----------------------------------ciemsys-------------------------------------
void SysInitialize(void)
{
    // ---A---Configuraci�n Oscilador (Capitulo 2 DataSheet: Oscillator Module)

    // --A.1--Configuraci�n oscilador interno (si es el caso)
    // Se usa cristal externo tipo HS-MP (4-16 MHz) de 10 MHz

    // --A.2--Habilitaci�n PLL (si es el caso)
    OSCTUNEbits.PLLEN = 1; // Fosc = 4 x 10 MHz = 40 MHz

    // ---B---Configuraci�n I/O Ports (Capitulo 10 DataSheet: I/O Ports)

    // --B.1--Inicializaci�n I/O Ports
    DISPLAY_LAT = 0; // Inicializar PORT para el display
    // Inicializar PORT de las l�neas de activaci�n de los displays
    EN_DISPLAY_LAT = 0;
    SW_S1_LAT = 0; // Inicializar PORT para el Switch SW_S1
    
    /* --B.2--Habilitaci�n buffer entrada digital de lineas digitales/anal�gicas
     * (Seccion 10.7 Datasheet: Port Analog Control)
     * S�lo cuando se desee configurar dichos pines como entradas digitales
     * ANSELX: PORTX Analog Select Register
     */
    SW_S1_ANSEL = 0; // Habilitaci�n buffer entrada digital para SW_S1

    // --B.3--Sentido I/O Ports
    DISPLAY_TRIS = 0; // L�neas para los 7 segementos como salidas
    EN_DISPLAY_TRIS &= 0b11000011; // L�neas activaci�n displays como salidas
    SW_S1_TRIS = 1; // L�nea del SW_S1 como entrada

    return;
}

//----------------------------------ciemsys-------------------------------------
void delayms(uint16_t u16_ms)
{
    for (uint16_t u16_n = 0; u16_n < u16_ms; u16_n++)
    {
        __delay_ms(1);
    }
    return;
}

/* *****************************************************************************
 * DISE�O DIGITAL CON MICROCONTROLADORES PIC DE 8 BITS
 * Profesor: Sebastian Puente Reyes, M.Sc.
 * - CIEMSYS -
 * Semillero de Investigaci�n en Inteligencia Computacional y Sistemas Embebidos
 * Escuela de Ingenier�a
 * Facultad de Cienc�as B�sicas e Ingenier�a
 * Universidad de los Llanos
 * Villavicencio - Meta, Colombia
 * *****************************************************************************
 */
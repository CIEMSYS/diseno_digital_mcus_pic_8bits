//----------------------------------ciemsys-------------------------------------
/*
 * File:        ProgrammedIO_08.c
 * ProjectName: ProgrammedIO_08
 * Course:      Dise�o Digital con Microcontroladores PIC de 8 bits
 * Topic:       I/O Ports - Conditional Programmed I/O
 * Processor:   PIC18F45K22
 * Compiler:    XC8, Ver.
 * Author:      Sebasti�n Fernando Puente Reyes, M.Sc.
 * E-mail:      sebastian.puente@unillanos.edu.co
 * Date:        Febrero de 2019
 */
//----------------------------------ciemsys-------------------------------------
/**
 * REQUERIMIENTO
 * --
 * Oscilador:
 *  Cristal Externo, tipo HS-MP, de 10 MHz
 *  Fosc = 10 MHz, Tosc = 1/Fosc = 100 nseg (0.1 useg)
 *  Tcy = 4/Fosc = 4*Tosc = 400 nseg (0.4 useg)
 * --
 * Descripci�n:
 * 
 * --
 * Ver Descripcion.txt
 */
//----------------------------------ciemsys-------------------------------------

/**
 * Secci�n: Archivos Incluidos
 */

#include <xc.h>
#include <stdint.h> // Lib est�ndar para enteros
#include "ConfigurationBits_HSMP.h" // Bits de configuraci�n

/**
 * Secci�n: Definiciones (macros)
 */

#define _XTAL_FREQ      (10000000) // Macro para __delay_ms(), etc

#define DISPLAY             (LATD)
#define DISPLAY_TRIS        (TRISD)
#define DISPLAY_LAT         (LATD)
#define EN_DISPLAY_LAT      (LATC)
#define EN_DISPLAY_TRIS     (TRISC)
#define EN_UNITS_DISPLAY    (LATCbits.LATC5) // Habilitador Display Unidades
#define EN_TENS_DISPLAY     (LATCbits.LATC4) // Habilitador Display Decenas
#define EN_HND_DISPLAY      (LATCbits.LATC3) // Habilitador Display Centenas
#define EN_THSD_DISPLAY     (LATCbits.LATC2) // Habilitador Display Unidades Mil
#define DISPLAY_OFF()       (EN_DISPLAY_LAT &= 0b11000011)
#define DISP(x)             (DISPLAY = u8_Disp7SegCC[x])

#define SW_S1           (PORTBbits.RB0)
#define SW_S1_TRIS      (TRISBbits.TRISB0)
#define SW_S1_LAT       (LATB)
#define SW_S1_ANSEL     (ANSELBbits.ANSB0)
#define PRESSED         (0)

/**
 * Secci�n: Prototipos de Funciones
 */

/**
 * SysInitialize()
 * @Resumen
 *  A. Configuraci�n oscilador
 *  B. Configuraci�n puertos I/O
 */
void SysInitialize(void);


void PrintDisplay(uint16_t Value);

/**
 * Secci�n: Variables Globales
 */

// Arreglo para el manejo de un display 7 segmentos de c�todo com�n
//                          0     1     2     3     4     5     6     7     8     9
uint8_t u8_Disp7SegCC[] = {0x3F, 0x06, 0x5B, 0x4F, 0x66, 0x6D, 0x7D, 0x07, 0x7F, 0x6F};

/**
 * Secci�n: Funci�n Principal (main)
 */

void main(void)
{
    // Variables locales
    uint16_t u16_Number = 3107;
    
    // --Configuraciones Iniciales
    SysInitialize();
    DISPLAY_OFF();
    
    // --Ciclo Infinito, Tareas que el MCU hace indefinidamente
    while (1)
    {
        if(SW_S1 == PRESSED)
        {
            // Mientras se mantenga pulsado SW_S1 se estar� haciendo un retardo
            while(SW_S1 == PRESSED)
            {
                __delay_ms(1);
            }
            u16_Number++;
        }
        PrintDisplay(u16_Number);
    }
    return;
}

/**
 * Secci�n: Desarrollo de Funciones
 */

//----------------------------------ciemsys-------------------------------------
/**
 * PrintDisplay((uint16_t Value)
 * @Resumen
 *  Muestra Valor en 4 display C.C. 7 seg. multiplexadoss
 * @Parametro Value: Valor a visualizar en los 4 displays
 */
void PrintDisplay(uint16_t Value)
{
    static uint8_t u8_Units = 0;
    static uint8_t u8_Tens = 0;
    static uint8_t u8_Hundreds = 0;
    static uint8_t u8_Thousands = 0;
    
    u8_Thousands = Value/1000;
    u8_Hundreds = (Value/100)%10;
    u8_Tens = (Value/10)%10;
    u8_Units = (Value/1)%10;
    
    EN_THSD_DISPLAY = 0;    // Deshabilitaci�n Display Unidades de Mil
    EN_HND_DISPLAY = 0;     // Deshabilitaci�n Display Centenas
    EN_TENS_DISPLAY = 0;    // Deshabilitaci�n Display Decenas
    EN_UNITS_DISPLAY = 1;   // Habilitaci�n Display Unidades
    DISP(u8_Units);
    __delay_ms(4);
    
    EN_THSD_DISPLAY = 0;
    EN_HND_DISPLAY = 0;
    EN_TENS_DISPLAY = 1;
    EN_UNITS_DISPLAY = 0;
    DISP(u8_Tens);
    __delay_ms(4);
    
    EN_THSD_DISPLAY = 0;
    EN_HND_DISPLAY = 1;
    EN_TENS_DISPLAY = 0;
    EN_UNITS_DISPLAY = 0;
    DISP(u8_Hundreds);
    __delay_ms(4);
    
    EN_THSD_DISPLAY = 1;
    EN_HND_DISPLAY = 0;
    EN_TENS_DISPLAY = 0;
    EN_UNITS_DISPLAY = 0;
    DISP(u8_Thousands);
    __delay_ms(4); 
}

//----------------------------------ciemsys-------------------------------------
void SysInitialize(void)
{
    // ---A---Configuraci�n Oscilador (Capitulo 2 DataSheet: Oscillator Module)

    // --A.1--Configuraci�n oscilador interno
    // Se usa cristal externo tipo HS-MP (4-16 MHz) de 10 MHz, Fosc = 10 MHz
    
    // --A.2--Habilitaci�n PLL
    //OSCTUNEbits.PLLEN = 1; //Fosc = 4 x XX MHz = YY MHz

    // ---B---Configuraci�n I/O Ports (Capitulo 10 DataSheet: I/O Ports)

    // --B.1--Inicializaci�n I/O Ports
    DISPLAY_LAT = 0; // Inicializar PORT para el display
    // Inicializar PORT de las l�neas de activaci�n de los displays
    EN_DISPLAY_LAT = 0;
    SW_S1_LAT = 0; // Inicializar PORT para el Switch SW_S1
    
    /* --B.2--Habilitaci�n buffer entrada digital de lineas digitales/anal�gicas
     * (Seccion 10.7 Datasheet: Port Analog Control)
     * S�lo cuando se desee configurar dichos pines como entradas digitales
     * ANSELX: PORTX Analog Select Register
     */
    SW_S1_ANSEL = 0; // Habilitaci�n buffer entrada digital para SW_S1
    
    // --B.3--Sentido I/O Ports
    DISPLAY_TRIS = 0x00; // L�neas para los 7 segementos como salidas
    EN_DISPLAY_TRIS &= 0b11000011; // L�neas activaci�n displays como salidas
    SW_S1_TRIS = 1; // L�nea del SW_S1 como entrada

    return;
}

/* *****************************************************************************
 * DISE�O DIGITAL CON MICROCONTROLADORES PIC DE 8 BITS
 * Profesor: Sebastian Puente Reyes, M.Sc.
 * - CIEMSYS -
 * - Grupo de Estudio en Inteligencia Computacional y Sistemas Embebidos
 * Escuela de Ingenier�a
 * Facultad de Cienc�as B�sicas e Ingenier�a
 * Universidad de los Llanos
 * Villavicencio - Meta, Colombia
 * *****************************************************************************
 */
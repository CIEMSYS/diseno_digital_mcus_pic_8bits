## Diseño Digital con Microcontroladores PIC de 8 bits

El presente repositorio pertenece al curso *Diseño Digital con Microcontroladores PIC de 8 bits* en actual elaboración por el [**Semillero de Investigación en Inteligencia Computacional y Sistemas Embebidos - CIEMSYS**](https://ciemsys.wordpress.com/) de la [Universidad de los Llanos](https://www.unillanos.edu.co/)

El microcontrolador utilizado, en los proyectos de éste curso, es el [PIC18F45K22](https://www.microchip.com/wwwproducts/en/PIC18F45K22) de Microchip.

---

### CIEMSYS PIC-8bit Development Board (Tarjeta de Desarrollo)

La [*Tarjeta de desarrollo CIEMSYS PIC-8bit*](https://bitbucket.org/CIEMSYS/diseno_digital_mcus_pic_8bits/src/master/CIEMSYS_PIC-8bit_DevBoard/v1/CIEMSYS_PIC-8bit_DevBoard_v1.pdf); diseñada por el Semillero de Investigación CIEMSYS, es propuesta y probada con todos los proyectos del presente repositorio. 

---

### MPLAB X IDE y Compilador C MPLAB XC8

El lenguaje utilizado en los diferentes proyectos es C usando el [compilador XC8 de Microchip](https://www.microchip.com/mplab/compilers) ISO C99. El compilador XC8 es gratuito y está disponible para su descarga en el sitio web de [Microchip Technology](https://www.microchip.com/mplab/compilers).

Todos los ejemplos del curso están creados como proyectos con el entorno de desarrollo MPLAB X IDE de Microchip. Su distribución es libre y gratuita y se puede [descargar](https://www.microchip.com/mplab/mplab-x-ide) directamente de el sitio oficial de Microchip.
Luego, tenga en cuenta que es necesario tener MPLAB X IDE instalado para poder acceder a dichos proyectos.